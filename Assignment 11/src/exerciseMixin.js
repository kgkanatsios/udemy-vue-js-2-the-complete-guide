export const exerciseMixin = {
    data() {
        return {
            exerciseFourOne : 'Exercise 4.1',
            exerciseFourTwo : 'Exercise 4.2'
        }
    },
    computed : {
        computedExerciseFourOne(){
            return this.exerciseFourOne.split("").reverse().join("");
        },
        computedExerciseFourTwo(){
            return this.exerciseFourTwo + ' (' + this.exerciseFourTwo.length + ')';
        }
    }
};